﻿using LS1500RLibrary_Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LS1500R_Framework
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        OptekLS1500R _optekLS1500R;


        #region Properties
        string _s;
        public string S
        {
            get { return _s; }
            set { _s = value; NotifyPropertyChanged(); }
        }

        int _b1;
        public int B1
        {
            get { return _b1; }
            set { _b1 = value; NotifyPropertyChanged(); }
        }

        int _b2;
        public int B2
        {
            get { return _b2; }
            set { _b2 = value; NotifyPropertyChanged(); }
        }

        int _b3;
        public int B3
        {
            get { return _b3; }
            set { _b3 = value; NotifyPropertyChanged(); }
        }

        int _b4;
        public int B4
        {
            get { return _b4; }
            set { _b4 = value; NotifyPropertyChanged(); }
        }

        int _b5;
        public int B5
        {
            get { return _b5; }
            set { _b5 = value; NotifyPropertyChanged(); }
        }

        int _b6;
        public int B6
        {
            get { return _b6; }
            set { _b6 = value; NotifyPropertyChanged(); }
        }

        int _b7;
        public int B7
        {
            get { return _b7; }
            set { _b7 = value; NotifyPropertyChanged(); }
        }

        int _b8;
        public int B8
        {
            get { return _b8; }
            set { _b8 = value; NotifyPropertyChanged(); }
        }

        int _b9;
        public int B9
        {
            get { return _b9; }
            set { _b9 = value; NotifyPropertyChanged(); }
        }

        int _b10;
        public int B10
        {
            get { return _b10; }
            set { _b10 = value; NotifyPropertyChanged(); }
        }

        string _assemblyVersionString;
        public string AssemblyVersionString
        {
            get { return _assemblyVersionString; }
            set { _assemblyVersionString = value; NotifyPropertyChanged(); }
        }


        public string IPAddress { get; set; } = "192.168.1.5";
        public int Port { get; set; } = 502;
        public int TransportRetries { get; set; } = 5;
        public int TransportReadTimeout { get; set; } = 50;
        public uint TransportRetryOnOldResponseThreshold { get; set; } = 3;
        public bool TransportSlaveBusyUsesRetryCount { get; set; } = true;
        public int TransportWaitToRetryMilliseconds { get; set; } = 5;


        bool _connected;
        public bool Connected
        {
            get { return _connected; }
            set { _connected = value; NotifyPropertyChanged(); }
        }

        bool _pollStatusBits = true;
        public bool PollStatusBits
        {
            get { return _pollStatusBits; }
            set { _pollStatusBits = value; NotifyPropertyChanged(); }
        }

        string _powerReading = "";
        public string PowerReading
        {
            get { return _powerReading; }
            set { _powerReading = value; NotifyPropertyChanged(); }
        }

        ushort _laserFrequencyForPowerMetering = 1000;
        public ushort LaserFrequencyForPowerMetering
        {
            get { return _laserFrequencyForPowerMetering; }
            set { _laserFrequencyForPowerMetering = value; NotifyPropertyChanged(); }
        }

        ushort _laserDutyCycleForPowerMetering = 35;
        public ushort LaserDutyCycleForPowerMetering
        {
            get { return _laserDutyCycleForPowerMetering; }
            set { _laserDutyCycleForPowerMetering = value; NotifyPropertyChanged(); }
        }

        public UInt32 StartPosition { get; set; }
        public ushort StripLength { get; set; }
        public double ForwardSpeed { get; set; }
        public ushort DutyCyclePercentage { get; set; }
        public ushort LaserFrequencyHz { get; set; }

        public ushort RecipeIndex { get; set; }
        #endregion


        private System.Timers.Timer _statusPoller = new System.Timers.Timer();

        public MainWindow()
        {
            _optekLS1500R = new OptekLS1500R();
            DataContext = this;
            InitializeComponent();

            AssemblyVersionString = _optekLS1500R.AssemblyVersionString;
            _statusPoller.Interval = 100; // Polling time interval
            _statusPoller.Elapsed += StatusPoller_Elapsed;

        }

        private void StatusPoller_Elapsed(object sender, ElapsedEventArgs e)
        {
            B1 = Convert.ToInt16(_optekLS1500R.GetSafetyCircuitOK());
            B2 = Convert.ToInt16(_optekLS1500R.GetLaserOK());
            B3 = Convert.ToInt16(_optekLS1500R.GetLaserTemperatureFault());
            B4 = Convert.ToInt16(_optekLS1500R.GetLaserVoltageFault());
            B5 = Convert.ToInt16(_optekLS1500R.GetExtractFlowOK());
            B6 = Convert.ToInt16(_optekLS1500R.GetEStopPressed());
            B7 = Convert.ToInt16(_optekLS1500R.GetExtractReady());
            B8 = Convert.ToInt16(_optekLS1500R.GetXReverseLimit());
            B9 = Convert.ToInt16(_optekLS1500R.GetXForwardLimit());
            B10 = Convert.ToInt16(_optekLS1500R.GetDoorSensor());
        }

        private void Connect_Click(object sender, RoutedEventArgs e)
        {
            if (_optekLS1500R.Connect(IPAddress, Port, TransportRetries, TransportReadTimeout, TransportRetryOnOldResponseThreshold, TransportSlaveBusyUsesRetryCount, TransportWaitToRetryMilliseconds))
            {
                Connected = true;
                if (PollStatusBits)
                    _statusPoller.Start();
            }
        }

        private void Disconnect_Click(object sender, RoutedEventArgs e)
        {
            if (_optekLS1500R.Disconnect())
            {
                Connected = false;
                _statusPoller.Stop();
            }
        }




        #region Writing process parameters to the PLC
        private void SetStartPosition_Click(object sender, RoutedEventArgs e)
        {
            _optekLS1500R.SetStartPosition(StartPosition);
        }

        private void SetStripLength_Click(object sender, RoutedEventArgs e)
        {
            _optekLS1500R.SetStripLength(StripLength);
        }

        private void SetScanSpeed_Click(object sender, RoutedEventArgs e)
        {
            _optekLS1500R.SetScanSpeed(ForwardSpeed);
        }

        private void SetLaserDutyCycle_Click(object sender, RoutedEventArgs e)
        {
            _optekLS1500R.SetLaserDutyCycle(DutyCyclePercentage);
        }

        private void SetLaserFrequency_Click(object sender, RoutedEventArgs e)
        {
            _optekLS1500R.SetLaserFrequency(LaserFrequencyHz);
        }
        #endregion




        #region Reading process parameters from the PLC
        private void GetStartPosition_Click(object sender, RoutedEventArgs e)
        {
            S = _optekLS1500R.GetStartPosition().ToString();
        }

        private void GetStripLength_Click(object sender, RoutedEventArgs e)
        {
            S = _optekLS1500R.GetStripLength().ToString();
        }

        private void GetScanSpeed_Click(object sender, RoutedEventArgs e)
        {
            S = _optekLS1500R.GetScanSpeed().ToString();
        }

        private void GetLaserDutyCycle_Click(object sender, RoutedEventArgs e)
        {
            S = _optekLS1500R.GetLaserDutyCycle().ToString();
        }

        private void GetLaserFrequency_Click(object sender, RoutedEventArgs e)
        {
            S = _optekLS1500R.GetLaserFrequency().ToString();
        }
        #endregion




        #region Reading System Information
        private void GetScreenNumber_Click(object sender, RoutedEventArgs e)
        {
            S = _optekLS1500R.GetScreenNumber().ToString();
        }

        private void GetFibresProcessed_Click(object sender, RoutedEventArgs e)
        {
            S = _optekLS1500R.GetFibresProcessed().ToString();
        }

        private void GetProcessStatus_Click(object sender, RoutedEventArgs e)
        {
            S = _optekLS1500R.GetProcessStatus().ToString();
        }

        private void GetCurrentRecipeName_Click(object sender, RoutedEventArgs e)
        {
            S = _optekLS1500R.GetCurrentRecipeName();
        }

        private void GetCurrentRecipeIndex_Click(object sender, RoutedEventArgs e)
        {
            S = _optekLS1500R.GetCurrentRecipeIndex().ToString();
        }
        #endregion




        #region Reading status bits
        private void GetSafetyCircuitOK_Click(object sender, RoutedEventArgs e)
        {
            B1 = Convert.ToInt16(_optekLS1500R.GetSafetyCircuitOK());
        }

        private void GetLaserOK_Click(object sender, RoutedEventArgs e)
        {
            B2 = Convert.ToInt16(_optekLS1500R.GetLaserOK());
        }

        private void GetLaserTemperatureFault_Click(object sender, RoutedEventArgs e)
        {
            B3 = Convert.ToInt16(_optekLS1500R.GetLaserTemperatureFault());
        }

        private void GetLaserVoltageFault_Click(object sender, RoutedEventArgs e)
        {
            B4 = Convert.ToInt16(_optekLS1500R.GetLaserVoltageFault());
        }

        private void GetExtractFlowOK_Click(object sender, RoutedEventArgs e)
        {
            B5 = Convert.ToInt16(_optekLS1500R.GetExtractFlowOK());
        }

        private void GetEStopPressed_Click(object sender, RoutedEventArgs e)
        {
            B6 = Convert.ToInt16(_optekLS1500R.GetEStopPressed());
        }

        private void GetExtractReady_Click(object sender, RoutedEventArgs e)
        {
            B7 = Convert.ToInt16(_optekLS1500R.GetExtractReady());
        }

        private void GetXReverseLimit_Click(object sender, RoutedEventArgs e)
        {
            B8 = Convert.ToInt16(_optekLS1500R.GetXReverseLimit());
        }

        private void GetXForwardLimit_Click(object sender, RoutedEventArgs e)
        {
            B9 = Convert.ToInt16(_optekLS1500R.GetXForwardLimit());
        }

        private void GetDoorSensor_Click(object sender, RoutedEventArgs e)
        {
            B10 = Convert.ToInt16(_optekLS1500R.GetDoorSensor());
        }

        private void GetAllStatusBits_Click(object sender, RoutedEventArgs e)
        {
            B1 = Convert.ToInt16(_optekLS1500R.GetSafetyCircuitOK());
            B2 = Convert.ToInt16(_optekLS1500R.GetLaserOK());
            B3 = Convert.ToInt16(_optekLS1500R.GetLaserTemperatureFault());
            B4 = Convert.ToInt16(_optekLS1500R.GetLaserVoltageFault());
            B5 = Convert.ToInt16(_optekLS1500R.GetExtractFlowOK());
            B6 = Convert.ToInt16(_optekLS1500R.GetEStopPressed());
            B7 = Convert.ToInt16(_optekLS1500R.GetExtractReady());
            B8 = Convert.ToInt16(_optekLS1500R.GetXReverseLimit());
            B9 = Convert.ToInt16(_optekLS1500R.GetXForwardLimit());
            B10 = Convert.ToInt16(_optekLS1500R.GetDoorSensor());
        }
        #endregion




        #region Starting the process and Recipe handling
        private void StartStripping_Click(object sender, RoutedEventArgs e)
        {
            _optekLS1500R.StartStripping();
        }

        private void SelectRecipe1to10_Click(object sender, RoutedEventArgs e)
        {
            _optekLS1500R.SelectRecipe1to10(RecipeIndex);
        }

        private void SaveCurrentRecipe_Click(object sender, RoutedEventArgs e)
        {
            _optekLS1500R.SaveCurrentRecipe();
        }
        #endregion



        #region Power Metering
        CancellationTokenSource cts = new CancellationTokenSource();
        private async void StartPowerMetering_Click(object sender, RoutedEventArgs e)
        {
            _optekLS1500R.StartPowerMetering();
            await CancellablePowerMeteringLoop(cts.Token);
        }

        private async Task CancellablePowerMeteringLoop(CancellationToken ct)
        {
            PowerReading = "Starting...";
            await Task.Delay(750);

            _optekLS1500R.SetLaserFrequencyForPowerMetering(LaserFrequencyForPowerMetering);
            _optekLS1500R.SetLaserDutyCycleForPowerMetering(LaserDutyCycleForPowerMetering);

            while (!ct.IsCancellationRequested)
            {
                PowerReading = await Task.Run(() => _optekLS1500R.TakeSinglePowerMeterReading().ToString());
                await Task.Delay(50);
            }

            PowerReading = "Cancelled...";
            await Task.Delay(3000);
            PowerReading = "";
        }

        private void StopPowerMetering_Click(object sender, RoutedEventArgs e)
        {
            cts.Cancel();
            cts = new CancellationTokenSource();
            _optekLS1500R.StopPowerMetering();
        }

        private void ZeroReading_Click(object sender, RoutedEventArgs e)
        {
            _optekLS1500R.ZeroPowerMeterReading();
        }
        #endregion











        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }


    }
}


