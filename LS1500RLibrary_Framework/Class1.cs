﻿using NModbus;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LS1500RLibrary_Framework
{
    public class OptekLS1500R
    {
        const ushort StartPositionAddress1_2 = 3019;           // Address 3020 - ushort first half
        const ushort StartPositionAddress2_2 = 3020;           // Address 3021 - ushort second half
        const ushort StripLengthAddress = 3021;           // Address 3022 - ushort first half
        const ushort ForwardSpeedAddress = 3022;           // Address 3023
        const ushort LaserDutyCycleAddress = 3026;           // Address 3027
        const ushort LaserFrequencyAddress = 3027;           // Address 3028

        AddressAndBit SafetyCircuitOKAddressAndBit = new AddressAndBit(0, 0);      // Address 1, bit 0
        AddressAndBit LaserOKAddressAndBit = new AddressAndBit(0, 4);      // Address 1, bit 4
        AddressAndBit LaserTemperatureFaultAddressAndBit = new AddressAndBit(0, 5);      // Address 1, bit 5
        AddressAndBit LaserVoltageFaultAddressAndBit = new AddressAndBit(0, 6);      // Address 1, bit 6
        AddressAndBit ExtractFlowOKAddressAndBit = new AddressAndBit(0, 7);      // Address 1, bit 7
        AddressAndBit EStopPressedAddressAndBit = new AddressAndBit(0, 12);     // Address 1, bit 12
        AddressAndBit ExtractReadyAddressAndBit = new AddressAndBit(0, 13);     // Address 1, bit 13
        AddressAndBit XReverseLimitAddressAndBit = new AddressAndBit(0, 9);      // Address 1, bit 9
        AddressAndBit XForwardLimitAddressAndBit = new AddressAndBit(0, 10);      // Address 1, bit 10
        AddressAndBit DoorSensorAddressAndBit = new AddressAndBit(1, 6);      // Address 2, bit 6

        AddressAndBit SelectRecipe1AddressAndBit = new AddressAndBit(67, 1);     // Address 68, bit 1
        AddressAndBit SelectRecipe2AddressAndBit = new AddressAndBit(67, 2);     // Address 68, bit 2
        AddressAndBit SelectRecipe3AddressAndBit = new AddressAndBit(67, 3);     // Address 68, bit 3
        AddressAndBit SelectRecipe4AddressAndBit = new AddressAndBit(67, 4);     // Address 68, bit 4
        AddressAndBit SelectRecipe5AddressAndBit = new AddressAndBit(67, 5);     // Address 68, bit 5
        AddressAndBit SelectRecipe6AddressAndBit = new AddressAndBit(67, 6);     // Address 68, bit 6
        AddressAndBit SelectRecipe7AddressAndBit = new AddressAndBit(67, 7);     // Address 68, bit 7
        AddressAndBit SelectRecipe8AddressAndBit = new AddressAndBit(67, 8);     // Address 68, bit 8
        AddressAndBit SelectRecipe9AddressAndBit = new AddressAndBit(67, 9);     // Address 68, bit 9
        AddressAndBit SelectRecipe10AddressAndBit = new AddressAndBit(67, 10);    // Address 68, bit 10
        AddressAndBit SaveCurrentRecipeAddressAndBit = new AddressAndBit(67, 11);    // Address 68, bit 11

        AddressAndBit StartStrippingAddressAndBit = new AddressAndBit(64, 7);     // Address 65, bit 7

        const ushort ScreenNumberAddress = 1000;           // Address 1
        const ushort FibresProcessedAddress = 1099;           // Address 100
        const ushort ProcessStatusAddress = 2029;           // Address 1030

        AddressAndRange CurrentRecipeNameAddressAndRange = new AddressAndRange(2999, 5);   // Address 3000-3004 (5 points), returns a string
        const ushort RecipeIndexAddress = 3004;           // Address 3005

        //For power metering:
        AddressAndBit StartTakingPowerReadingsAddressAndBit = new AddressAndBit(69, 8);     // Address 70, bit 8
        AddressAndBit ZeroPowerMeterReadingAddressAndBit = new AddressAndBit(69, 9);     // Address 70, bit 9
        const ushort PowerMeterReading = 1199;           // Address 1200
        const ushort LaserFrequencyForPowerMeteringAddress = 1370;           // Address 1371
        const ushort LaserDutyCycleForPowerMeteringAddress = 1369;           // Address 1370

        //Decimal conversion factors that are needed for the PLC since the customer requested more decimal places 
        const ushort scanSpeedDecimalConversionPower = 1; // 10^1 = 10
        const ushort dutyCycleDecimalConversionPower = 2; // 10^2 = 100

        IModbusMaster master;
        TcpClient tcpClient;
        object modbusLocker = new object();
        System.Timers.Timer _keepAlivePoller = new System.Timers.Timer();

        public string AssemblyVersionString => Assembly.GetExecutingAssembly().GetName().Version.ToString();

        public OptekLS1500R(string iPAddress, int port, int transportRetries, int transportReadTimeout, uint transportRetryOnOldResponseThreshold, bool transportSlaveBusyUsesRetryCount, int transportWaitToRetryMilliseconds)
        {
            Connect(iPAddress, port, transportRetries, transportReadTimeout, transportRetryOnOldResponseThreshold, transportSlaveBusyUsesRetryCount, transportWaitToRetryMilliseconds);
            _keepAlivePoller.Elapsed += _keepAlivePoller_Elapsed;
        }

        public OptekLS1500R()
        {
            _keepAlivePoller.Elapsed += _keepAlivePoller_Elapsed;
        }

        ~OptekLS1500R()
        {
            Disconnect();
        }

        public bool Connect(string iPAddress, int port, int transportRetries, int transportReadTimeout, uint transportRetryOnOldResponseThreshold, bool transportSlaveBusyUsesRetryCount, int transportWaitToRetryMilliseconds)
        {
            try
            {
                if (master != null)
                    master.Dispose();
                if (tcpClient != null)
                    tcpClient.Close();

                tcpClient = new TcpClient();
                IAsyncResult asyncResult = tcpClient.BeginConnect(iPAddress, port, null, null);
                asyncResult.AsyncWaitHandle.WaitOne(3000, true); //wait for 3 sec
                if (!asyncResult.IsCompleted)
                {
                    tcpClient.Close();
                    return false;
                }

                // create Modbus TCP Master by the tcpclient
                ModbusFactory factory = new ModbusFactory();

                master = factory.CreateMaster(tcpClient);
                master.Transport.Retries = transportRetries;
                master.Transport.ReadTimeout = transportReadTimeout;
                master.Transport.RetryOnOldResponseThreshold = transportRetryOnOldResponseThreshold;
                master.Transport.SlaveBusyUsesRetryCount = transportSlaveBusyUsesRetryCount;
                master.Transport.WaitToRetryMilliseconds = transportWaitToRetryMilliseconds;

                _keepAlivePoller.Start();
                _keepAlivePoller.Interval = 1000;

                if (!tcpClient.Connected)
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                Debug.Write(ex);
                throw;
            }
        }

        public bool Disconnect()
        {
            try
            {
                if (_keepAlivePoller != null)
                    _keepAlivePoller.Stop();
                if (tcpClient != null)
                    tcpClient.Dispose();
                if (master != null)
                    master.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                Debug.Write(ex);
                throw;
            }
        }

        private void _keepAlivePoller_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            GetSafetyCircuitOK();
        }






        #region Get/set holding register
        private void SetInt16(ushort Index, ushort val, int retries = 2) // Write single register. Read holding register to check value has been set.
        {
            int attempt = 0;
            bool completed = false;

            while (!completed)
            {
                try
                {
                    lock (modbusLocker)
                        master.WriteSingleRegister(1, Index, val);

                    ushort data;

                    lock (modbusLocker)
                        data = master.ReadHoldingRegisters(1, Index, 1)[0];

                    completed = data == val;

                    if (!completed)
                        throw new Exception("Failed to set register " + Index);
                }
                catch (Exception ex)
                {
                    attempt++;
                    if (attempt > retries)
                        throw;
                }
            }
        }

        private ushort GetInt16(ushort Index) // Read single register.
        {
            lock (modbusLocker)
                return master.ReadHoldingRegisters(1, Index, 1)[0];
        }

        private string GetString(ushort Index, ushort numberOfPoints) // Read multiple registers.
        {
            lock (modbusLocker)
            {
                ushort[] us = master.ReadHoldingRegisters(1, Index, numberOfPoints); //array size = numberOfPoints = 5. Note 8224 is empty

                List<char> charList = new List<char>();

                foreach (ushort pair in us)
                {
                    byte[] bytes = BitConverter.GetBytes(pair); // each "us" variable contains 2 bytes (2 characters) => each recipe can contain up to 10 characters since numberOfPoints is 5
                    foreach (byte b in bytes.Reverse())
                    {
                        charList.Add(Convert.ToChar(b));
                    }
                }

                return new string(charList.ToArray());
            }
        }
        #endregion



        #region Bit management
        private (ushort, ushort) ConvertUInt32ToUShorts(UInt32 ui)
        {
            ushort us2 = (ushort)(ui >> 16);
            ushort us1 = (ushort)(ui & 0xFFFF); // The number: 65535 

            return (us2, us1);
        }

        private UInt32 ConvertUShortsToUInt32(ushort us2, ushort us1)
        {
            UInt32 ui = (UInt32)((us2 << 16) | us1);
            return ui;
        }

        private bool ReadSelectedBitFromHoldingRegister(ushort Index, ushort selectedBit)
        {
            return Convert.ToBoolean((GetInt16(Index) >> selectedBit) & 1);
        }

        private void WriteSelectedBitToHoldingRegister(ushort Index, ushort bit, ushort selectedBit)
        {
            ushort previousValue = GetInt16(Index);

            if (bit == 1)
                SetInt16(Index, (ushort)(previousValue | (1 << selectedBit)));
            else if (bit == 0)
                SetInt16(Index, (ushort)(previousValue & ~(1 << selectedBit))); // the operator ~ reverses each bit in a value. Also selectedBit-1
        }
        #endregion



        #region Writing process parameters to the PLC
        ///<summary>
        ///<para>Set the start position (um) for the current recipe.</para>
        ///<para>Min: 10000um Max: 120000um.</para>
        ///</summary>
        ///<returns>void</returns>
        public void SetStartPosition(uint startPosition) // change argument to to ushort according to manual. therefore change the relevant helper methods
        {
            if (startPosition > 120000 || startPosition < 10000)
                throw new Exception("Start Position Exceeds Range");

            (ushort startPosition2_2, ushort startPosition1_2) = ConvertUInt32ToUShorts(startPosition);

            SetInt16(StartPositionAddress2_2, Convert.ToUInt16(startPosition2_2));
            SetInt16(StartPositionAddress1_2, Convert.ToUInt16(startPosition1_2));
        }

        ///<summary>
        ///<para>Set the strip lwngth (um) for the current recipe.</para>
        ///<para>Min: 0um Max: 40000um.</para>
        ///</summary>
        ///<returns>void</returns>
        public void SetStripLength(ushort stripLength) // change argument to to ushort according to manual. therefore change the relevant helper methods
        {
            if (stripLength > 40000 || stripLength < 0)
                throw new Exception("Strip Length Exceeds Range");

            SetInt16(StripLengthAddress, stripLength);
        }

        ///<summary>
        ///<para>Set the scan speed (mm/s) for the current recipe.</para>
        ///<para>Min: 0mm/s Max: 40mm/s.</para>
        ///</summary>
        ///<returns>void</returns>
        public void SetScanSpeed(double forwardSpeed)
        {
            if (forwardSpeed > 40 || forwardSpeed < 0)
                throw new Exception("Laser Frequency Exceeds Range");

            SetInt16(ForwardSpeedAddress, RemoveDecimalPlaceFromDouble(forwardSpeed, 1)); // correction for extra decimal places requested by the customer
        }

        ///<summary>
        ///<para>Set the laser duty cycle (%) for the current recipe.</para>
        ///<para>Min: 1% Max: 2000%.</para>
        ///</summary>
        ///<returns>void</returns>
        public void SetLaserDutyCycle(ushort dutyCyclePercentage)
        {
            if (dutyCyclePercentage > 70 || dutyCyclePercentage < 0)
                throw new Exception("Duty Cycle Exceeds Range");

            SetInt16(LaserDutyCycleAddress, dutyCyclePercentage);
        }

        ///<summary>
        ///<para>Set the laser frequency (Hz) for the current recipe.</para>
        ///<para>Min: 1Hz Max: 2000Hz.</para>
        ///</summary>
        ///<returns>void</returns>
        public void SetLaserFrequency(ushort laserFrequencyHz)
        {
            if (laserFrequencyHz > 2000 || laserFrequencyHz < 0)
                throw new Exception("Laser Frequency Exceeds Range");

            SetInt16(LaserFrequencyAddress, laserFrequencyHz);
        }
        #endregion



        #region Reading process parameters from the PLC
        ///<summary>
        ///<para>Returns the start position of the current recipe.</para>
        ///</summary>
        ///<returns>uint</returns>
        public uint GetStartPosition()
        {
            return ConvertUShortsToUInt32(Convert.ToUInt16(GetInt16(StartPositionAddress2_2)), Convert.ToUInt16(GetInt16(StartPositionAddress1_2)));
        }

        ///<summary>
        ///<para>Returns the strip length of the current recipe.</para>
        ///</summary>
        ///<returns>ushort</returns>
        public ushort GetStripLength()
        {
            return GetInt16(StripLengthAddress);
        }

        ///<summary>
        ///<para>Returns the scan speed of the current recipe.</para>
        ///</summary>
        ///<returns>double</returns>
        public double GetScanSpeed()
        {
            return (double)(GetInt16(ForwardSpeedAddress) / Math.Pow(10, scanSpeedDecimalConversionPower));
        }

        ///<summary>
        ///<para>Returns the laser duty cycle of the current recipe.</para>
        ///</summary>
        ///<returns>ushort</returns>
        public ushort GetLaserDutyCycle()
        {
            return GetInt16(LaserDutyCycleAddress);
        }

        ///<summary>
        ///<para>Returns the laser freqeuncy of the current recipe.</para>
        ///</summary>
        ///<returns>ushort</returns>
        public ushort GetLaserFrequency()
        {
            return GetInt16(LaserFrequencyAddress);
        }
        #endregion



        #region Reading System Information
        ///<summary>
        ///<para>Returns an integer denoting the current screen number for the Proface HMI.</para>
        ///</summary>
        ///<returns>ushort</returns>
        public ushort GetScreenNumber()
        {
            return GetInt16(ScreenNumberAddress);
        }

        ///<summary>
        ///<para>Returns the total number of fibres processed.</para>
        ///</summary>
        ///<returns>ushort</returns>
        public ushort GetFibresProcessed()
        {
            return GetInt16(FibresProcessedAddress);
        }

        ///<summary>
        ///<para>Returns the process status as an integer.</para>
        ///</summary>
        ///<returns>ushort</returns>
        public ushort GetProcessStatus()
        {
            return GetInt16(ProcessStatusAddress);
        }

        ///<summary>
        ///<para>Returns the current recipe name.</para>
        ///</summary>
        ///<returns>string</returns>
        public string GetCurrentRecipeName()
        {
            return GetString(CurrentRecipeNameAddressAndRange.Address, CurrentRecipeNameAddressAndRange.Range);
        }

        ///<summary>
        ///<para>Returns an integer which is the current recipe index (between 1 and 10).</para>
        ///</summary>
        ///<returns>ushort</returns>
        public ushort GetCurrentRecipeIndex()
        {
            return GetInt16(RecipeIndexAddress);
        }
        #endregion



        #region Reading status bits
        public bool GetSafetyCircuitOK()
        {
            return ReadSelectedBitFromHoldingRegister(SafetyCircuitOKAddressAndBit.Address, SafetyCircuitOKAddressAndBit.Bit);
        }

        public bool GetLaserOK()
        {
            return ReadSelectedBitFromHoldingRegister(LaserOKAddressAndBit.Address, LaserOKAddressAndBit.Bit);
        }

        public bool GetLaserTemperatureFault()
        {
            return ReadSelectedBitFromHoldingRegister(LaserTemperatureFaultAddressAndBit.Address, LaserTemperatureFaultAddressAndBit.Bit);
        }

        public bool GetLaserVoltageFault()
        {
            return ReadSelectedBitFromHoldingRegister(LaserVoltageFaultAddressAndBit.Address, LaserVoltageFaultAddressAndBit.Bit);
        }

        public bool GetExtractFlowOK()
        {
            return ReadSelectedBitFromHoldingRegister(ExtractFlowOKAddressAndBit.Address, ExtractFlowOKAddressAndBit.Bit);
        }

        public bool GetEStopPressed()
        {
            return ReadSelectedBitFromHoldingRegister(EStopPressedAddressAndBit.Address, EStopPressedAddressAndBit.Bit);
        }

        public bool GetExtractReady()
        {
            return ReadSelectedBitFromHoldingRegister(ExtractReadyAddressAndBit.Address, ExtractReadyAddressAndBit.Bit);
        }

        public bool GetXReverseLimit()
        {
            return ReadSelectedBitFromHoldingRegister(XReverseLimitAddressAndBit.Address, XReverseLimitAddressAndBit.Bit);
        }

        public bool GetXForwardLimit()
        {
            return ReadSelectedBitFromHoldingRegister(XForwardLimitAddressAndBit.Address, XForwardLimitAddressAndBit.Bit);
        }

        public bool GetDoorSensor()
        {
            return ReadSelectedBitFromHoldingRegister(DoorSensorAddressAndBit.Address, DoorSensorAddressAndBit.Bit);
        }
        #endregion



        #region Starting the process and Recipe handling
        ///<summary>
        ///<para>Begins the stripping process with the process parameters in the currently selected recipe.</para>
        ///</summary>
        ///<returns>void</returns>
        public void StartStripping()
        {
            WriteSelectedBitToHoldingRegister(StartStrippingAddressAndBit.Address, 1, StartStrippingAddressAndBit.Bit);
        }

        ///<summary>
        ///<para>Select a recipe (between 1 and 10).</para>
        ///</summary>
        ///<returns>void</returns>
        public void SelectRecipe1to10(ushort recipeIndex)
        {
            if (recipeIndex < 1 || recipeIndex > 10)
                return;

            DeselectAllRecipes();

            switch (recipeIndex)
            {
                case 1:
                    WriteSelectedBitToHoldingRegister(SelectRecipe1AddressAndBit.Address, 1, SelectRecipe1AddressAndBit.Bit);
                    break;
                case 2:
                    WriteSelectedBitToHoldingRegister(SelectRecipe2AddressAndBit.Address, 1, SelectRecipe2AddressAndBit.Bit);
                    break;
                case 3:
                    WriteSelectedBitToHoldingRegister(SelectRecipe3AddressAndBit.Address, 1, SelectRecipe3AddressAndBit.Bit);
                    break;
                case 4:
                    WriteSelectedBitToHoldingRegister(SelectRecipe4AddressAndBit.Address, 1, SelectRecipe4AddressAndBit.Bit);
                    break;
                case 5:
                    WriteSelectedBitToHoldingRegister(SelectRecipe5AddressAndBit.Address, 1, SelectRecipe5AddressAndBit.Bit);
                    break;
                case 6:
                    WriteSelectedBitToHoldingRegister(SelectRecipe6AddressAndBit.Address, 1, SelectRecipe6AddressAndBit.Bit);
                    break;
                case 7:
                    WriteSelectedBitToHoldingRegister(SelectRecipe7AddressAndBit.Address, 1, SelectRecipe7AddressAndBit.Bit);
                    break;
                case 8:
                    WriteSelectedBitToHoldingRegister(SelectRecipe8AddressAndBit.Address, 1, SelectRecipe8AddressAndBit.Bit);
                    break;
                case 9:
                    WriteSelectedBitToHoldingRegister(SelectRecipe9AddressAndBit.Address, 1, SelectRecipe9AddressAndBit.Bit);
                    break;
                case 10:
                    WriteSelectedBitToHoldingRegister(SelectRecipe10AddressAndBit.Address, 1, SelectRecipe10AddressAndBit.Bit);
                    break;
            }
        }

        private void DeselectAllRecipes()
        {
            WriteSelectedBitToHoldingRegister(SelectRecipe1AddressAndBit.Address, 0, SelectRecipe1AddressAndBit.Bit);
            WriteSelectedBitToHoldingRegister(SelectRecipe2AddressAndBit.Address, 0, SelectRecipe2AddressAndBit.Bit);
            WriteSelectedBitToHoldingRegister(SelectRecipe3AddressAndBit.Address, 0, SelectRecipe3AddressAndBit.Bit);
            WriteSelectedBitToHoldingRegister(SelectRecipe4AddressAndBit.Address, 0, SelectRecipe4AddressAndBit.Bit);
            WriteSelectedBitToHoldingRegister(SelectRecipe5AddressAndBit.Address, 0, SelectRecipe5AddressAndBit.Bit);
            WriteSelectedBitToHoldingRegister(SelectRecipe6AddressAndBit.Address, 0, SelectRecipe6AddressAndBit.Bit);
            WriteSelectedBitToHoldingRegister(SelectRecipe7AddressAndBit.Address, 0, SelectRecipe7AddressAndBit.Bit);
            WriteSelectedBitToHoldingRegister(SelectRecipe8AddressAndBit.Address, 0, SelectRecipe8AddressAndBit.Bit);
            WriteSelectedBitToHoldingRegister(SelectRecipe9AddressAndBit.Address, 0, SelectRecipe9AddressAndBit.Bit);
            WriteSelectedBitToHoldingRegister(SelectRecipe10AddressAndBit.Address, 0, SelectRecipe10AddressAndBit.Bit);
        }

        ///<summary>
        ///<para>Saves the current recipe.</para>
        ///</summary>
        ///<returns>void</returns>
        public void SaveCurrentRecipe()
        {
            WriteSelectedBitToHoldingRegister(SaveCurrentRecipeAddressAndBit.Address, 1, SaveCurrentRecipeAddressAndBit.Bit);
        }
        #endregion



        #region Power Metering
        ///<summary>
        ///<para>Starts the power metering procedure. This includes diverting the beam and firing the laser. The TakeSinglePowerMeterReading() method should be called after this.</para>
        ///</summary>
        ///<returns>void</returns>
        public void StartPowerMetering()
        {
            WriteSelectedBitToHoldingRegister(StartTakingPowerReadingsAddressAndBit.Address, 0, StartTakingPowerReadingsAddressAndBit.Bit);
            WriteSelectedBitToHoldingRegister(StartTakingPowerReadingsAddressAndBit.Address, 1, StartTakingPowerReadingsAddressAndBit.Bit);
        }

        ///<summary>
        ///<para>Takes a single reading from the power meter. This method is best called in a loop.</para>
        ///</summary>
        ///<returns>ushort</returns>
        public ushort TakeSinglePowerMeterReading()
        {
            return GetInt16(PowerMeterReading);
        }

        ///<summary>
        ///<para>Stops power metering procedure.</para>
        ///</summary>
        ///<returns>void</returns>
        public void StopPowerMetering()
        {
            WriteSelectedBitToHoldingRegister(StartTakingPowerReadingsAddressAndBit.Address, 0, StartTakingPowerReadingsAddressAndBit.Bit);
        }

        ///<summary>
        ///<para>Set the laser frequency (Hz) for power metering.</para>
        ///<para>Min: 1Hz Max: 2000Hz</para>
        ///</summary>
        ///<returns>void</returns>
        public void SetLaserFrequencyForPowerMetering(ushort laserFrequencyHzForPowerMetering)
        {
            if (laserFrequencyHzForPowerMetering > 2000 || laserFrequencyHzForPowerMetering < 0)
                throw new Exception("Laser Frequency Exceeds Range");

            SetInt16(LaserFrequencyForPowerMeteringAddress, laserFrequencyHzForPowerMetering);
        }

        ///<summary>
        ///<para>Set the laser duty cycle (%) for power metering.</para>
        ///<para>Min: 1% Max: 70%</para>
        ///</summary>
        ///<returns>void</returns>
        public void SetLaserDutyCycleForPowerMetering(ushort laserDutyCyclePercentageForPowerMetering)
        {
            if (laserDutyCyclePercentageForPowerMetering > 70 || laserDutyCyclePercentageForPowerMetering < 0)
                throw new Exception("Duty Cycle Exceeds Range");

            SetInt16(LaserDutyCycleForPowerMeteringAddress, laserDutyCyclePercentageForPowerMetering);
        }

        ///<summary>
        ///<para>Zero the power meter reading.</para>
        ///<para>Note: zeroing will not work while the laser is firing.</para>
        ///</summary>
        ///<returns>void</returns>
        public void ZeroPowerMeterReading()
        {
            WriteSelectedBitToHoldingRegister(ZeroPowerMeterReadingAddressAndBit.Address, 0, ZeroPowerMeterReadingAddressAndBit.Bit);
            WriteSelectedBitToHoldingRegister(ZeroPowerMeterReadingAddressAndBit.Address, 1, ZeroPowerMeterReadingAddressAndBit.Bit);
        }
        #endregion


        private ushort RemoveDecimalPlaceFromDouble(double d, int numberOfDecimalPlacesAllowed) // Because of how the PLC is using uints to represent floats
        {
            double input = d;

            Console.WriteLine("Input double: " + input);

            double multiplier = Math.Pow(10, numberOfDecimalPlacesAllowed); // e.g 10 or 100

            // Check if input has correct number of decimal places
            if (Math.Round(input * multiplier) != input * multiplier)
            {
                // If not, round it to correct number of decimal places
                input = (float)Math.Round(input, numberOfDecimalPlacesAllowed, MidpointRounding.ToEven);
            }

            // Convert to a ushort by multiplying by the multiplier and casting to ushort
            ushort output = (ushort)(input * multiplier);


            Console.WriteLine("Output ushort: " + output);

            return output;
        }
    }

    #region Support classes
    public class AddressAndBit
    {
        public ushort Address;
        public ushort Bit;

        public AddressAndBit(ushort address, ushort bit)
        {
            Address = address;
            Bit = bit;
        }
    }

    public class AddressAndRange
    {
        public ushort Address;
        public ushort Range;

        public AddressAndRange(ushort address, ushort range)
        {
            Address = address;
            Range = range;
        }
    }
    #endregion
}

